﻿namespace ImporterExporter
{
    public class TextData : Data
    {
        public string Text { get; set; }

        public TextData(string text)
        {
            Text = text;
        }
    }
}
