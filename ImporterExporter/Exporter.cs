﻿namespace ImporterExporter
{
    public abstract class Exporter
    {
        public abstract Data ExportData { get; set; }
    }
}
