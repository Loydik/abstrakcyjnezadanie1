﻿namespace ImporterExporter
{
    public class TextImporter : Importer
    {
        public string ImportedText
        {
            get
            {
                TextData data = ImportedData as TextData;
                return data?.Text;
            }
        }
    }
}
