﻿namespace ImporterExporter
{
    public class Importer
    {
        public Data ImportedData { get; set; }

        public void ImportData(Data data)
        {
            ImportedData = data;
        }
    }
}
