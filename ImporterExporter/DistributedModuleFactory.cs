﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImporterExporter
{
    public abstract class DistributedModuleFactory
    {
        public abstract Data CreateData();
        public abstract Exporter CreateExporter();
        public abstract Importer CreateImporter();
    }
}
