﻿namespace ImporterExporter
{
    public sealed class TextExporter : Exporter
    {
        private TextData _exportData;

        public override Data ExportData
        {
            get
            {
                var temp = new TextData(_exportData.Text);
                _exportData.Text = string.Empty;

                return temp; 
            }
            set { _exportData = value as TextData; }
        }

        public TextExporter(string text)
        {
            _exportData = new TextData(text);
        }
    }
}
