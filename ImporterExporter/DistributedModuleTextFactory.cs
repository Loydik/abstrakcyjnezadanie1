﻿namespace ImporterExporter
{
    public class DistributedModuleTextFactory : DistributedModuleFactory
    {
        private string Text { get; }

        public DistributedModuleTextFactory(string text)
        {
            Text = text;
        }

        public override Data CreateData()
        {
            TextData textData = new TextData(Text);
            return textData;
        }

        public override Exporter CreateExporter()
        {
            TextExporter exporter = new TextExporter(Text);
            return exporter;
        }

        public override Importer CreateImporter()
        {
            TextImporter importer = new TextImporter();
            return importer;
        }
    }
}
